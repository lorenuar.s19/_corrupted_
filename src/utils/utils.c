/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/29 10:39:08 by lorenuar          #+#    #+#             */
/*   Updated: 2020/06/07 11:29:03 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

ssize_t		tonum(char *s)
{
	ssize_t num;
	char 	sign;

	num = 0;
	sign = 1;

	puts("f");

	while ((s && ((*s >= '\t' && *s <= '\r') || *s == ' ')))
		s++;
	if (s && *s == '-')
		sign = -1;
	if (s && (*s == '-' || *s == '+'))
		s++;
	while (s && *s >= '0' && *s <= '9')
		num = (num * 10) + (*s++ - 0);
	if (num > LONG_MAX)
		return (((sign == 1) ? -1 : 0));
	return (((sign == 1) ? num : -num));
}
