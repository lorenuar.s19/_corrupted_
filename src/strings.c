/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strings.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/29 10:06:50 by lorenuar          #+#    #+#             */
/*   Updated: 2020/06/07 11:20:00 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

size_t str_len(char *s)
{
	size_t len;
	
	len = 0;
	while (s && s[len])
		len++;
	return (len);
}

void n_str(char *s, size_t n)
{
	while (n-- > 0)
	{
		write(1, s, str_len(s));
	}
}

void	p_str(char *s, char *fmt)
{
	size_t pfx;

	pfx = tonum(fmt);

	printf("pfx %lu\n", pfx);
}
