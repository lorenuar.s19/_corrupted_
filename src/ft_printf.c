/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/29 09:52:05 by lorenuar          #+#    #+#             */
/*   Updated: 2020/06/07 11:42:09 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

#define D(x) printf("%4d in %10s | " #x, __LINE__, __FUNCTION__);

size_t	hasto(char *s, char c)
{
	size_t	to;

	to = 0;
	while (s && s[to])
	{
		if (s[to] == c)
			return (to + 1);
		to++;
	}
	if (s && s[to] == c)
		return (to + 1);
	return (0);
}

int ft_printf(char *fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	while (fmt && *fmt)
	{
		if (*++fmt == '%')
		{
			if (*fmt == 's')
			{
				char *s = va_arg(args, char *);
				D(FOUND_S);
				p_str(s, fmt);
				fmt++;
			}
			else
			{
				fmt++;
			}
		}
		else
		{
			
		}
	}
	va_end(args);
	return (0);
}
