/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/29 10:09:39 by lorenuar          #+#    #+#             */
/*   Updated: 2020/06/07 11:23:57 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <stdarg.h>
# include <stdlib.h>
# include <unistd.h>
# include <stdio.h>
# include <limits.h>

int			ft_printf(char *fmt, ...);

size_t		str_len(char *s);
void		n_str(char *s, size_t n);
void		p_str(char *s, char *fmt);
ssize_t 	tonum(char *s);


#endif
