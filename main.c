/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/29 10:11:39 by lorenuar          #+#    #+#             */
/*   Updated: 2020/06/07 11:27:39 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

#define TEST "%7s", "Test"

int main(int argc, char *argv[])
{
	//ft_printf("Vargtest %s %c %s \n", "Hello", "World");

	//printf(".% 6s.\n", "1234567");

	// int sw;
	// int i;

	// sw = 0;
	// sw = (sw == 0) ? 1 : sw;
	// while (((sw == 0) ? 1 : ((sw >= 0) ? (i <= sw) : (i >= sw))))
	// {
	// 	printf("i : %d\n", i);
	// 	(sw >= 0) ? i++ : i-- ;
	// }

	// n_str("0", 1000);

	//printf(TEST);

	
	ft_printf(TEST);
	return (0);
}
